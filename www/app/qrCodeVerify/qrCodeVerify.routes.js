angular.module('qrCodeVerifyModule')
.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
	$stateProvider
		.state('qrCodeVerifyForm', {
			url: '/qr-code-verify',
			templateUrl: 'app/qrCodeVerify/templates/qrCodeVerifyForm.tpl.html',
			controller: 'qrCodeVerifyFormController',
			controllerAs: 'qrCodeVerifyForm'
		})
		// .state('productInfo', {
		// 	url: '/productInfo',
		// 	templateUrl: 'app/qrCodeVerify/templates/productInfo.tpl.html',
		// 	controller: 'productInfoController',
		// 	controllerAs: 'productInfo'
		// })
	; /*$stateProvider closing*/
}]);
