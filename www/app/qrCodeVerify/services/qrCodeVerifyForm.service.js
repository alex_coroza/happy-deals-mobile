angular.module('qrCodeVerifyModule')
.factory('qrCodeVerifyForm', ['$http', function($http) {

	var f = this;

	// sample data only
	var basePath = 'http://192.168.0.111/happydeals/';
	var merchantId = 1;
	var qrCode = 'Yt68YgOGHUT';


	f.getMerchantInfo = function() {
		return $http.post(basePath+'backend/controllers/validateQrCode.php', { request: 'getMerchantInfo', merchantId: merchantId });
	};




	f.verifyQrCode = function() {
		return $http.post(basePath+'backend/controllers/validateQrCode.php', { request: 'verifyQrCode', qrCode: qrCode });
	};




	f.checkAccountCredentials = function(userName, password) {
		return $http.post(basePath+'backend/controllers/validateQrCode.php', { request: 'checkAccountCredentials', userName: userName, password: password });
	};




	f.getQrCode = function() {
		return qrCode;
	};










	return f;

}]);