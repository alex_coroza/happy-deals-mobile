angular.module('qrCodeVerifyModule')
.controller('qrCodeVerifyFormController', ['$q', 'qrCodeVerifyForm', '$state', function($q, qrCodeVerifyForm, $state) {

	console.log('qrCodeVerifyFormController initialized!');

	var vm = this;
	vm.merchantInfo = {};


	// get merchantInfo
	qrCodeVerifyForm.getMerchantInfo()
	.then(function(response) {
		vm.merchantInfo = response.data;
	});




	// check credentials first
	// and then perform qr code verification
	vm.verifyQrCode = function() {
		qrCodeVerifyForm.checkAccountCredentials(vm.userName, vm.password)
		.then(function(response) {
			if(response.data == 'true') {
				return qrCodeVerifyForm.verifyQrCode();
			} else {
				return $q.reject('The credentials you entered is invalid!');
			}
		}).then(function(response) {
			if(response.data == '"already processed"') {
				alert('Verification failed because QR code was already redeemed!');
			} else if(response.data == '"not found"') {
				alert('Verification failed because QR code is invalid!');
			} else {
				alert(qrCodeVerifyForm.getQrCode()+' was sucessfuly redeemed!');
				$state.reload();
				// $state.go('productInfo');
			}
		}).catch(function(errorMessage) {
			alert(errorMessage);
		});
	};










}]);