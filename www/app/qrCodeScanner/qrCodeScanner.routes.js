angular.module('qrCodeScannerModule')
.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
	$stateProvider
		.state('qrCodeScanner', {
			url: '/qr-code-scanner',
			templateUrl: 'app/qrCodeScanner/templates/qrCodeScanner.tpl.html',
			controller: 'qrCodeScannerController',
			controllerAs: 'qrCodeScanner'
		})
	; /*$stateProvider closing*/
}]);
